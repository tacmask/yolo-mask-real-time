from tracker.centroidtracker import CentroidTracker
from tracker.trackableobject import TrackableObject
from imutils.video import VideoStream
from imutils.video import FPS
import numpy as np
import argparse
import imutils
import time
import dlib #Kernelized correlation filter
import cv2
from pytorchYOLOv4.models import *
from pytorchYOLOv4.tool.utils import *

input_file = 0
output_file = 'output/salida1.mp4'
conf = 0.4
skip_frames = 15



print("[INFO] Abriendo video...")
vs = cv2.VideoCapture(input_file)

# Inicializar el video writer
writer = None

# Dimensiones de la imagen se extraen del primer frame
W = None
H = None

# Instanciar el tracker
ct = CentroidTracker(maxDisappeared=40, maxDistance=50)

#Lista de objetos y diccionario para los ID's
trackers = []
trackableObjects = {}
colors = [(0, 255, 0), (0, 0, 255)]
labels = ['Mask', 'No mask']

# Numero total de frames, numero de objetos que fueron hacia abajo y hacia arriba
total_no_mask = 0
total_mask = 0
totalFrames = 0


n_classes = 2
weightfile = 'pytorchYOLOv4/checkpoints/Yolov4_epoch50.pth'
namesfile = 'pytorchYOLOv4/test/_classes.txt'

# Iniciar el estimador de fps
fps = FPS().start()

# Bucle sobre los frames del video
while True:
  #Obtener el frame del video
  _,frame = vs.read()
  
	# Si el frame es vacio, se acabo el video
  if frame is None:
    break
  
	# Escalar el ancho a 500 pixels y transformar a RGB
  frame = imutils.resize(frame, width=500)
  rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

  if W is None or H is None:
    (H, W) = frame.shape[:2]

	# Inicializar el writer
  if writer is None:
    fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
    writer = cv2.VideoWriter(output_file, fourcc, 30, (W, H), True)
  
	# Inicializar el estado de la aplicacion, junto con la lista de rectangulos
  # ya sea del detector o del tracker
  status = "Waiting"
  rects = []
  class_idxs = []

	# Chequear si debemos correr el detector o el tracker
  if totalFrames % skip_frames == 0:
		# Correr el detector
    status = "Detecting"
    trackers = []
    class_idx_list = []
    model = Yolov4(n_classes=n_classes)

    pretrained_dict = torch.load(weightfile, map_location=torch.device('cuda'))
    model.load_state_dict(pretrained_dict)

    use_cuda = 1
    if use_cuda:
      model.cuda()

    img = Image.fromarray(rgb)
    sized = img.resize((608, 608))

    boxes = do_detect(model, sized, 0.4, n_classes, 0.4, use_cuda)

# Iterar sobre las detecciones
    for i in np.arange(len(boxes)):
      # Computer las coordenadas del rectangulo de deteccion
      box = boxes[i]
      startX = int((box[0] - box[2] / 2.0) * W)
      startY = int((box[1] - box[3] / 2.0) * H)
      endX = int((box[0] + box[2] / 2.0) * W)
      endY = int((box[1] + box[3] / 2.0) * H)
      class_idx = box[6]
      # Usar dlib para crear un tracker sobre el objeto detectado
      tracker = dlib.correlation_tracker()
      rect = dlib.rectangle(startX, startY, endX, endY)
      tracker.start_track(rgb, rect)

      # Acumular el tracker
      trackers.append(tracker)
      class_idx_list.append(class_idx)
# Sino utilizamos el tracker
  else:
    for (tracker, class_idx) in zip(trackers,class_idx_list):
      status = "Tracking"
      tracker.update(rgb)
      pos = tracker.get_position()

			# unpack the position object
      startX = int(pos.left())
      startY = int(pos.top())
      endX = int(pos.right())
      endY = int(pos.bottom())

			# Añadir el bounding box a la lista de rectangulos
      rects.append((startX, startY, endX, endY))
      class_idxs.append(class_idx)
	# Actualizar informacion de los centroides de los bbox
  objects = ct.update(rects)

	# Bucle sobre objetos trackeados
  for ((objectID, centroid), rect, class_idx) in zip(objects.items(), rects, class_idxs):
		# Buscar si el objeto trackeado existe con ese ID 
    to = trackableObjects.get(objectID, None)

		# Si no existe objeto, crear uno
    if to is None:
      to = TrackableObject(objectID, centroid)

		# Sino, existe objeto y se puede usar para calcular direccion
    else:
      to.centroids.append(centroid)

			# Chequear si el objeto ha sido contado o no
      if not to.counted:
				#  Si tiene mascarilla
        if class_idx == 0:
          total_mask += 1
          to.counted = True

				#  Si no tiene mascarilla
        elif class_idx == 1:
          total_no_mask += 1
          to.counted = True

		# almacenar objeto en trackeables
    trackableObjects[objectID] = to

		# Dibujar el ID y el centroide
    text = "{} (ID {})".format(labels[class_idx], objectID)
    cv2.putText(frame, text, (rect[0], rect[1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, colors[class_idx], 2)
    cv2.rectangle(frame, (rect[0], rect[1]), (rect[2], rect[3]), colors[class_idx], 1)
    

  info = [
    ("No mask", total_no_mask, (0, 0, 255)),
    ("Mask", total_mask, (0, 255, 0)),
    ("Estado", status, (0, 255, 255)),
  ]

	# Dibujar en el frame, la informacion
  for (i, (k, v, c)) in enumerate(info):
    text = "{}: {}".format(k, v)
    cv2.putText(frame, text, (10, H - ((i * 20) + 20)), cv2.FONT_HERSHEY_SIMPLEX, 0.4, c, 1)
  
  if writer is not None:
    writer.write(frame)

  if cv2.waitKey(1) & 0xFF == ord('q'):
    break
  cv2.imshow('frame', frame)	
  # Actualizar el FPS
  totalFrames += 1
  fps.update()
  

# Detener el FPS
fps.stop()
print("[INFO] Tiempo transcurrido: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

if writer is not None:
  writer.release()

vs.release()
