# yolo-mask-real-time
### Requirements
- Python3.6+
- virtualenv (`pip install virtualenv`)

### Installation
- `virtualenv env`
- `source env/bin/activate` (Linux)
- `pip install -r requirements.txt`
- `!git clone https://github.com/roboflow-ai/pytorch-YOLOv4.git`
- `pip install -r pytorch-YOLOv4/requirements.txt`
- `mv pytorch-YOLOv4 pytorchYOLOv4 `

### Execution
- `python mask_detector.py`
